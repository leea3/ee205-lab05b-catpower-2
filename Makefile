###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 05b - Cat Power 2 - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Makefile for  Cat Power 2
###
### @author  Arthur Lee <leea3@hawaii.edu>
### @date    12 Feb 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

#gcc for C, g++ for C++
CC     = g++
CFLAGS = -g -Wall -Wextra

TARGET = catPower

all: $(TARGET)

catPowerInstructions.o: catPowerInstructions.cpp catPowerInstructions.h
	$(CC) $(CFLAGS) -c catPowerInstructions.cpp

ev.o: ev.cpp ev.h units.h
	$(CC) $(CFLAGS) -c ev.cpp

megaton.o: megaton.cpp megaton.h units.h
	$(CC) $(CFLAGS) -c megaton.cpp

gge.o: gge.cpp gge.h units.h
	$(CC) $(CFLAGS) -c gge.cpp

foe.o: foe.cpp foe.h units.h
	$(CC) $(CFLAGS) -c foe.cpp

cat.o: cat.cpp cat.h units.h
	$(CC) $(CFLAGS) -c cat.cpp

catPower.o: catPower.cpp ev.h megaton.h gge.h foe.h cat.h
	$(CC) $(CFLAGS) -c catPower.cpp

catPower: catPower.o catPowerInstructions.o ev.o megaton.o gge.o foe.o cat.o
	$(CC) $(CFLAGS) -o $(TARGET) catPowerInstructions.o catPower.o ev.o megaton.o gge.o foe.o cat.o

clean:
	rm -f $(TARGET) *.o

test: catPower
	@./catPower 3.14 j j | grep -q "3.14 j is 3.14 j"
	@./catPower 3.14 j e | grep -q "3.14 j is 1.95983E+19 e"
	@./catPower 3.14 j m | grep -q "3.14 j is 7.50478E-16 m"
	@./catPower 3.14 j g | grep -q "3.14 j is 2.58862E-08 g"
	@./catPower 3.14 j f | grep -q "3.14 j is 3.14E-44 f"
	@./catPower 3.14 j c | grep -q "3.14 j is 0 c"
	@./catPower 3.14 j x |& grep -q "Invalid toUnit, enter a valid one"
	@./catPower 3.14 x j |& grep -q "Invalid fromUnit, enter a valid one"

	@echo "All tests pass"
