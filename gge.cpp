/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// cpp file containing implementation for gallon of gasoline conversions
///
/// @author Arthur Lee <leea3@hawaii.edu>
/// @date 12 Feb 2022
/////////////////////////////////////////////////////////////////////////////

#include "units.h"
#include "gge.h"

double fromGallonsOfGasolineToJoule( double gasoline ) {
   return gasoline / ONE_GALLON_GASOLINE_IN_A_JOULE;
}

double fromJouleToGallonsOfGasoline( double joule ) {
   return joule * ONE_GALLON_GASOLINE_IN_A_JOULE;
}

