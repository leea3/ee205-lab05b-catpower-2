/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file units.h
/// @version 1.0
///
/// Header file containing the unit conversions
///
/// @author Arthur Lee <leea3@hawaii.edu>
/// @date 12 Feb 2022
/////////////////////////////////////////////////////////////////////////////

#pragma once

const double ELECTRON_VOLTS_IN_A_JOULE = 6.24150974e18;
const double MEGATONS_IN_A_JOULE = 1 / (4.184e15);
const double ONE_GALLON_GASOLINE_IN_A_JOULE = 1 / (1.213e8);
const double FOE_IN_A_JOULE = 1 / (1e44);
const double CATPOWER_IN_A_JOULE = 0;

const char JOULE         = 'j';
const char ELECTRON_VOLT = 'e';
const char MEGATON = 'm';
const char ONE_GALLON_GASOLINE = 'g';
const char FOE = 'f';
const char CAT_POWER = 'c';
