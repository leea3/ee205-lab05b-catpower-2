/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file megaton.cpp
/// @version 1.0
///
/// cpp file containing implementaton for megaton conversions
///
/// @author Arthur Lee <leea3@hawaii.edu>
/// @date 12 Feb 2022
/////////////////////////////////////////////////////////////////////////////

#include "units.h"
#include "megaton.h"

double fromMegatonsToJoule( double megaton ) {
   return megaton / MEGATONS_IN_A_JOULE;
}

double fromJouleToMegatons( double joule ) {
   return joule * MEGATONS_IN_A_JOULE;
}

