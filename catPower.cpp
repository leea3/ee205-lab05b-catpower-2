///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// Note: CatPower 2 is a modularized version of CatPower
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file catPower.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Arthur Lee <leea3@hawaii.edu>
/// @date   14 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include "units.h"
#include "ev.h"
#include "megaton.h"
#include "gge.h"
#include "foe.h"
#include "cat.h"
#include "catPowerInstructions.h"


int main( int argc, char* argv[] ) {

   //terminates program if it's invoked improperly
   if( argc != 4 )catPowerInstructions();

   double fromValue;
   char   fromUnit;
   char   toUnit;

   fromValue = atof( argv[1] );
   fromUnit = argv[2][0];         // Get the first character from the second argument
   toUnit = argv[3][0];           // Get the first character from the thrid argument
   
#ifdef DEBUG
   //debug statements
   printf( "fromValue = [%lG]\n", fromValue ); 
   printf( "fromUnit = [%c]\n", fromUnit ); 
   printf( "toUnit = [%c]\n", toUnit ); 
#endif

   double commonValue;
   switch( fromUnit ) {
      case JOULE                 : commonValue = fromValue; // No conversion necessary
                                   break;
      case ELECTRON_VOLT         : commonValue = fromElectronVoltsToJoule( fromValue );
                                   break;
      case MEGATON               : commonValue = fromMegatonsToJoule( fromValue );
                                   break;
      case ONE_GALLON_GASOLINE   : commonValue = fromGallonsOfGasolineToJoule( fromValue);
                                   break;
      case FOE                   : commonValue = fromFoeToJoule( fromValue );
                                   break;
      case CAT_POWER             : commonValue = fromCatPowerToJoule( fromValue );;
                                   break;
      default                    : printf("Invalid fromUnit, enter a valid one\n");
                                   return 1;
                                   break;
   }

#ifdef DEBUG
   //debug statements
   printf( "commonValue = [%lG] joule\n", commonValue );
#endif

   double toValue;

   switch( toUnit ) {
      case JOULE                 : toValue = commonValue; // no conversion needed
                                   break;
      case ELECTRON_VOLT         : toValue = fromJouleToElectronVolts( commonValue );
                                   break;
      case MEGATON               : toValue = fromJouleToMegatons( commonValue );
                                   break;
      case ONE_GALLON_GASOLINE   : toValue = fromJouleToGallonsOfGasoline( commonValue );
                                   break;
      case FOE                   : toValue = fromJouleToFoe( commonValue );
                                   break;
      case CAT_POWER             : toValue = fromJouleToCatPower( commonValue );
                                   break;
      default                    : printf("Invalid toUnit, enter a valid one\n");
                                   return 1;
                                   break;
   }

   printf( "%lG %c is %lG %c\n", fromValue, fromUnit, toValue, toUnit );
}
