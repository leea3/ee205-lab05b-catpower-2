//////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file ev.cpp
/// @version 1.0
///
/// cpp file containing implementation for electronvolt conversions
///
/// @author Arthur Lee <leea3@hawaii.edu>
/// @date 12 Feb 2022
/////////////////////////////////////////////////////////////////////////////

#include "units.h"
#include "ev.h"

double fromElectronVoltsToJoule( double electronVolts ) {
   return electronVolts / ELECTRON_VOLTS_IN_A_JOULE ;
}

double fromJouleToElectronVolts( double joule ) {
   return joule * ELECTRON_VOLTS_IN_A_JOULE;
}

