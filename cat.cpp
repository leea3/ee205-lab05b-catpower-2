/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.cpp
/// @version 1.0
///
/// cpp file containing implementation for cat conversions
///
/// @author Arthur Lee <leea3@hawaii.edu>
/// @date 12 Feb 2022
/////////////////////////////////////////////////////////////////////////////

#include "units.h"
#include "cat.h"

double fromCatPowerToJoule( double catPower ) {
   return catPower * 0;  // Cats do no work
}

double fromJouleToCatPower( double joule ) {
   return joule * 0; // Cats do no work
}

