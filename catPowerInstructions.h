/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catpowerInstructions.h
/// @version 1.0
///
/// Header file containing declarations for catPower program instructions
///
/// @author Arthur Lee <leea3@hawaii.edu>
/// @date 12 Feb 2022
/////////////////////////////////////////////////////////////////////////////

#pragma once

extern int catPowerInstructions();
